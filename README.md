# Projet T4 : Incarner le président d'une université dans le but de l'améliorer le plus possible

[Lien vers le google doc/cahier des charges](https://docs.google.com/document/d/19wGSAEHV11wonVaTVJpAFzUGXdpcZM_sejWdv1Aa32k/edit?usp=sharing)

[Lien de téléchargement de l'exécutable](https://drive.google.com/open?id=1rDkMdZsVv2inw8_kYV8hDu9lfldSORoJ)

### Le MVC de notre prototype :

![MVCproto](img/mvcProto.png)

### Le pôles de gestion utilisé dans le prototype sont :

- la qualité de vie du personnel
- la qualité de vie de étudiants
- le taux de réussite

### Influence des investissements dans les différents pôles à gérer :

- modifie le budget annuel (concernant les frais d'inscription et les appels à projet)
- modifie le taux de réussite
- possibilité de se faire démettre de ses fonctions
- possibilité d'influencer les frais d'inscription
- possibilité d'influencer le nombre d'étudiant

### Finalité de la simulation :

A la fin de la simulation, vous obtiendrez une note /20 qui refletera la moyenne des scores obtenus dans les 3 pôles ci dessus au regard de la politique que vous avez mené.

### Nos p'tits conseils

Lors du début de vos projets T3, beaucoup d'entre vous choisirons UNITY comme outils de développement car l'outil peut être très agicheur, mais pour ce jeu nous vous conseillons d'y réfléchir à 2 fois et si il ne serait pas plus efficace/sympa de coder sur Visual Studio .net C#.

### Crédits:

J'vous jure y pas de crédits que des débi(l)ts. (on veut bien vos paypals en mp)
