using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPrezTige{

  public class Menu
  {

    public Menu()
    {

    }


    /*
    *  Procedures
    */

    //Affiche le reusltat en fin de jeu.
    public static void Afficher_resultat()
    {
      //Recuperation des informations
      int note = Gestionnaire.Resultat();


      //Affichage
      Console.Out.WriteLine("_____________Fin de Partie_____________");
      Console.Out.WriteLine();

      Console.Out.WriteLine("Bravo vous avez réussit à tenir jusqu'au bout!");
      Console.Out.WriteLine();
      Console.Out.WriteLine("Votre note est : "+note+"/20");



    }

    //Affiche les informations du Tour actuel
    public static void Afficher_Tour()
        {
            List<double> critereActuel = Gestionnaire.Critere_actuel();
      //Recuperation des informations
      double num_tour=critereActuel[0];
      double budget= critereActuel[1];
      double frais_inscription= critereActuel[2];
      double qualite_etu= critereActuel[3];
      double qualite_personel= critereActuel[4];
      double taux_reussite = critereActuel[5];
      double nbr_etudiant = critereActuel[9];

            //remplacer les 0 par les donnée de la liste

            //affichage
            Console.Out.WriteLine("_____________Information_____________");

      Console.Out.WriteLine();
      Console.Out.WriteLine("Année : "+num_tour);
      Console.Out.WriteLine("Budget en Millions d'Euro : " + budget);
      Console.Out.WriteLine("Fond de roulement en Millions d'Euro : " + budget/12);
            Console.Out.WriteLine("Nombre d'etudiants : " + nbr_etudiant);
            Console.Out.WriteLine("Frais d'inscription en Euro : " + frais_inscription);
      Console.Out.WriteLine("Taux de réussite en %: "+taux_reussite);
      Console.Out.WriteLine("Qualité de vie des etudiants en % : "+qualite_etu);
      Console.Out.WriteLine("Qualité de vie du personel en % : "+qualite_personel);
            Console.Out.WriteLine();

            Console.Out.WriteLine("Investissement pour ameliorer la qualité de vie du Etudiant Millions d'Euro : " + critereActuel[6]);
            Console.Out.WriteLine("Investissement pour ameliorer la qualité de vie du personel Millions d'Euro : " + critereActuel[7]);
            Console.Out.WriteLine("Investissement pour ameliorer le taux de réussite Millions d'Euro : " + critereActuel[8]) ;



        }

        //Affiche les choix possible pour le joueur et lui demande quel choix il prend
        public static void Afficher_Choix(){

            Console.Out.WriteLine("_____________Liste des Choix_____________");
            Console.Out.WriteLine();
            Console.Out.WriteLine("1) Voir le budget prévisionnel");
            Console.Out.WriteLine("2) Modifier les frais d'inscription");
            Console.Out.WriteLine("3) Investir dans la qualité de vie des étudiants");
            Console.Out.WriteLine("4) Investir dans la qualité de vie du personnel");
            Console.Out.WriteLine("5) Investir dans les taux de réussite");
            Console.Out.WriteLine("6) Passer au tour suivant");
            Console.Out.WriteLine();
            Console.Out.WriteLine("Veuillez entrer le numéro de votre choix");
            int choix = int.Parse(Console.ReadLine());
            Afficher_DemandeChoix(choix);

    }

    //Après que le joueur a fais son choix il doit entrer une valeur.
    public static void Afficher_DemandeChoix(int choix)
    {
            List<int> l = new List<int> { 2,3,4,5 };
            double valeur=0;
      try
      {
             if (l.Contains(choix)){
                Console.Out.WriteLine();
                Console.Out.WriteLine("Entrez la valeur souhaitée :");
                valeur = double.Parse(Console.ReadLine());
             }

                switch (choix){
                    case 1:
                        double budgetp = Gestionnaire.BudgetPrevisionel();
                        Console.Out.WriteLine("______Budget prévisionel (En Millions d'Euro)______");
                        Console.Out.WriteLine();
                        Console.Out.WriteLine(budgetp);
                        Console.Out.WriteLine();
                        break;                        
                    case 2:
                        Gestionnaire.Modif_FraisInscription(valeur);
                        break;
                    case 3:
                        Gestionnaire.Modif_InvestQualiterEtu(valeur);
                        break;

                    case 4:
                        Gestionnaire.Modif_InvestQualiterPersonel(valeur);
                        break;                        
                    case 5:
                        Gestionnaire.Modif_InvestTauxReussite(valeur);
                        break;
                    case 6:
                        Gestionnaire.TourSuivant();
                        Console.Out.WriteLine("===================================================================");
                        Menu.VerifGameOver();


                        break;
                }

      }
      catch(Exception e)
      {
        Console.Out.WriteLine("Une Erreur est survenue : Valeur is not valid -> {0}", e.Source);
      }

    }

    //Affiche les notifications du jeu
    public static void Afficher_Notif()
    {

      if(Gestionnaire.Check_roulement())
      {
        Console.Out.WriteLine("_____________Attention_____________");
        Console.Out.WriteLine();
        Console.Out.WriteLine("Votre budget est en dessous du fond de roulement... Veillez à ne pas refaire la même erreur sous peine d'expulsion!");
        Console.Out.WriteLine("Conseil : Augmentez les frais d'inscription ou licenciez du personel pour compenser le trou dans les caisses de l'université!");

      }
      Console.Out.WriteLine();
    }

    //Affiche la fin de partie
    public static void Afficher_GameOver()
    {
      Console.Out.WriteLine("_____________GAME OVER_____________");
      Console.Out.WriteLine();
      Console.Out.WriteLine("Vous avez été licencié de votre poste par le recteur de l'académie.");
      Console.Out.WriteLine("vous n'avez pas respecté les fonds de roulement ou alors vos stats étaient trop basses!");
      Console.Out.WriteLine();
      Console.Out.WriteLine("...");
      Console.Out.WriteLine();
      Console.Out.WriteLine("git gud :]");
      Console.ReadLine();
    }

    //Demande le nom du joueur et le retourne
    public static void Demande_Nom()
        {
            Console.Out.WriteLine("_____________Bienvenue_____________");
            Console.Out.WriteLine();
            Console.Out.WriteLine("Bienvenue Mr.Wemmert vous avez été élu par le CA en tant que président de l'université, vous devrez gérer le budget des différents pôles de l'université afin d'obtenir la meilleure note de satisfaction au terme de votre mandat!");
            Console.Out.WriteLine("Faites attention, respectez le fond du roulement!");

        }


    public static void VerifGameOver()
        {
            if(Gestionnaire.Verif_Condition())
            {
                Console.Clear();
                Afficher_GameOver();
                Environment.Exit(0);
            }
        }
    
        //affiche le bilan de fin de partie
    public static void FinPartie()
        {
            Console.Out.WriteLine("_____________Fin de Partie_____________");
            Console.Out.WriteLine();
            Console.Out.WriteLine("Votre mandat est arrivé à terme !");
            Console.Out.WriteLine("Voici votre note :");
            double note = Gestionnaire.Bilan();
            Console.Out.WriteLine("{0}/20", note);
            Console.ReadLine();
        }

    }//Fin class Menu



}
