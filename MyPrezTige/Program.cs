﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPrezTige
{
    class Program
    {
        static void Main(string[] args)
        {
            List<double> criteres_depart;
            criteres_depart = new List<double>();

            Gestionnaire.Genere_critere(criteres_depart);
            Universite universite = new Universite(1,500, criteres_depart[1], criteres_depart[2], criteres_depart[3], 400);
            President president = new President("Wemmert", false, false);
            Gestionnaire g = new Gestionnaire(universite, president);
            
            int tour = universite.Annees;

            //Affichage des criteres et budget 
            Menu.Demande_Nom();
            while (tour < 4){
                tour = universite.Annees;
                Menu.Afficher_Notif();
                Menu.Afficher_Tour();                
                Menu.Afficher_Choix();
            }
            Menu.FinPartie();
        }
    }
}
