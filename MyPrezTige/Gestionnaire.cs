﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPrezTige
{
    
    public class Gestionnaire
    {
        public static List<double> Criteres_performance { get; set; }
        private static readonly Random rnd = new Random();
        public  static Universite Un { get; set; }
        public  static President P { get; set; }


        public Gestionnaire(Universite universite, President president)
        {
            Gestionnaire.Un = universite;
            Gestionnaire.P = president;

        }

        //donne les valeurs du premier tour pour la qualité du personnel, qualité vie étudiante, taux de réussite, prestige
        public static void Genere_critere(List<double> criteres_depart)
        {
            int min = 45;
            int max = 55;

            for(int i=0; i<4; i++)
            {
                criteres_depart.Add(rnd.Next(min, max));
            }
        }

        //récupère les critères et budget d'une université
        public static List<double> Critere_actuel()
        {
            List<double> criteres = new List<double>();

            for(int i=0;i<10; i++)
            {
                switch (i)
                {
                    case 0:
                        criteres.Add(Un.Annees);
                        break;
                    case 1:
                        criteres.Add(Un.Budget);
                        break;
                    case 2:
                        criteres.Add(Un.Frais_inscription);
                        break;
                    case 3:
                        criteres.Add(Un.Qualite_etudiante);
                        break;
                    case 4:
                        criteres.Add(Un.Qualite_personel);
                        break;
                    case 5:
                        criteres.Add(Un.Taux_reussite);
                        break;
                    case 6:
                        criteres.Add(Un.Invest_Q_Etudiant);
                        break;
                    case 7:
                        criteres.Add(Un.Invest_Q_Personel);
                        break;
                    case 8:
                        criteres.Add(Un.Invest_Q_Taux_reussite);
                        break;
                    case 9:
                        criteres.Add(Un.NbEtudiant);
                        break;
                        
                }
            }

            return criteres;
        }


        /*
         *  CODE AJOUTER PAR RABIE
         */


        //Renvoie les bool des check
        public static bool Check_roulement() {

            double budgetdepense = Un.Budget - (Un.Invest_Q_Personel + Un.Invest_Q_Etudiant + Un.Invest_Q_Taux_reussite);

            if (budgetdepense < Un.Budget_avant/12)
            {               
                    P.Danger_roulement = true;               
                
            }
            return P.Danger_roulement;
        }

        //Calcule le Resultat et renvoie une note sur 20
        public static int Resultat()
        {
            return 1;
        }

        //Procedure de modification
        public static void Modif_FraisInscription(double valeur)
        {
            Un.Frais_inscription = valeur;
        }

        public static void Modif_InvestTauxReussite(double valeur)
        {
            Un.Invest_Q_Taux_reussite = valeur;
        }

        public static void Modif_InvestQualiterEtu(double valeur)
        {
            Un.Invest_Q_Etudiant = valeur;
        }

        public static void Modif_InvestQualiterPersonel(double valeur)
        {
            Un.Invest_Q_Personel = valeur;
        }


        public static double BudgetPrevisionel()
        {   //Budget previsionel = Budget - les dépense + l'entrée des frais d'inscription
            Un.Budget_avant = Un.Budget;
            double a = 320 * Un.Taux_reussite / 35;
            double res = Un.Budget - (Un.Invest_Q_Personel + Un.Invest_Q_Etudiant + Un.Invest_Q_Taux_reussite) + (Un.Frais_inscription * Un.NbEtudiant)/1000000+ a;
            return res;
        }


        public static bool Verif_Condition()
        {
            bool res = false;
            double budgetdepense = Un.Budget - (Un.Invest_Q_Personel + Un.Invest_Q_Etudiant + Un.Invest_Q_Taux_reussite);


            if (P.Danger_roulement==true && budgetdepense < Un.Budget_avant/12)
            {
                res = true;
            }
            
            if(Un.Taux_reussite<=0 || Un.Qualite_etudiante <= 0 ||Un.Qualite_personel <= 0)
            {
                res = true;
            }

            return res;
        }

        public static void TourSuivant()
        {
            if (Un.Frais_inscription <= 100)
            {
                Un.NbEtudiant *= 1.01;
                Un.Qualite_etudiante *= 1.01;
                Un.Taux_reussite *= 0.99;
            }
            else if (Un.Frais_inscription > 10000)
            {
                Un.NbEtudiant *= 0.7;
                Un.Qualite_etudiante *= 0.7;
                Un.Taux_reussite *= 1.01;
            }

            Un.Qualite_etudiante += Un.Invest_Q_Etudiant / 10;
        
            if (Un.Qualite_etudiante >= 70)
            {
                Un.Taux_reussite *= 0.95;
                Un.NbEtudiant *= 1.05;
            }
            else if (Un.Qualite_etudiante < 30)
            {
                Un.NbEtudiant *= 0.99;
            }

            Un.Qualite_personel += Un.Invest_Q_Personel / 20;

            if (Un.Qualite_personel >= 90)
            {
                Un.Taux_reussite *= 1.01;
            }
            else if (Un.Qualite_personel < 40)
            {
                Un.Taux_reussite *= 0.95;
            }

            Un.Taux_reussite += Un.Invest_Q_Taux_reussite / 100;

            if (Un.Taux_reussite >= 85)
            {
                Un.Qualite_etudiante *= 1.05;
            }
            else if (Un.Taux_reussite < 50)
            {
                Un.Qualite_personel *= 0.9;
            }
            Un.Budget = BudgetPrevisionel();

            Un.Invest_Q_Taux_reussite = 0;
            Un.Invest_Q_Personel = 0;
            Un.Invest_Q_Etudiant = 0;

            Un.Annees++;
        }

        //calcule la note finale du joueur
        public static double Bilan()
        {
            List<double> cara = new List<double>();
            cara = Critere_actuel();

            double etudiant = cara[3] * 20 / 100;
            double personel = cara[4] * 20 / 100;
            double reussite = cara[5] * 20 / 100;

            double note = (etudiant + personel + reussite) / 3;

            return note;
        }
    }




}
