﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPrezTige
{
    public class Universite
    {
        public int Annees { get; set; }
        public double Budget { get; set; }
        public double Qualite_etudiante { get; set; }
        public double Qualite_personel { get; set; }
        public double Taux_reussite { get; set; }
        public double Frais_inscription { get; set; }
        public double NbEtudiant { get; set; } = 46000;

        public double Invest_Q_Etudiant { get; set; } = 0;
        public double Invest_Q_Personel { get; set; } = 0;
        public double Invest_Q_Taux_reussite { get; set; } = 0;

        public double Budget_avant { get; set; } = 0;

        public Universite(int annees, double budget, double etudiante, double personelle, double reussite, double inscription)
        {
            this.Annees = annees;
            this.Budget = budget;
            this.Qualite_etudiante = etudiante;
            this.Qualite_personel = personelle;
            this.Taux_reussite = reussite;
            this.Frais_inscription = inscription;
        }     
    }
}
