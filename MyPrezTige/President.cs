﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPrezTige
{
    public class President
    {
        public string Nom { get; set; }
        public bool Danger_roulement { get; set; }
        public bool Danger_critique { get; set; }
        public int compteur { get; set; } = 0;

        public President(string nom, bool roulement, bool critique)
        {
            this.Nom = nom;
            this.Danger_critique = critique;
            this.Danger_roulement = roulement;
        }
    }
}